import React from 'react'
import Map from './components/Map'
import Reactotron from 'reactotron-react-native'

Reactotron.configure({ host: '10.10.20.215', name: 'rocketUber' }).useReactNative().connect()

Reactotron.warn('Iniciando aplicação')

const App = () => <Map />

export default App