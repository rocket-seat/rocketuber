import { Platform, PixelRatio } from 'react-native'

// export const apikey = "AIzaSyDy9paZM7CsKtd1IkQl--vwcJXkDiVS03U"
export const apikey = "AIzaSyB1O8amubeMkw_7ok2jUhtVj9IkME9K8sc"

export function getPixelSize(pixels) {
    return Platform.select({
        ios: pixels,
        android: PixelRatio.getPixelSizeForLayoutSize(pixels)
    })
}