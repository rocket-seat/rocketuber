import { StyleSheet, Platform } from 'react-native'

export const styles = StyleSheet.create({
    LocationBox: {
        backgroundColor: '#FFF',
        shadowColor: '#000',
        shadowOffset: { x: 0, y: 0 },
        shadowOpacity: 0.1,
        elevation: 1,
        borderRadius: 3,
        flexDirection: 'row',
        marginTop: Platform.select({ ios: 20, android: 10 }),
        marginLeft: Platform.select({ ios: 0, android: 10 }),
    },

    LocationText: {
        marginTop: 8,
        marginBottom: 8,
        marginRight: 10,
        marginLeft: 10,
        fontSize: 14,
        color: '#333',
    },

    LocationTimeBox: {
        backgroundColor: '#222',
        paddingTop: 3,
        paddingBottom: 3,
        paddingRight: 8,
        paddingLeft: 8,
    },

    LocationTimeText: {
        color: '#FFF',
        fontSize: 12,
        textAlign: 'center',
    },

    LocationTimeTextSmall: {
        color: '#FFF',
        fontSize: 10,
        textAlign: 'center',
    }
})