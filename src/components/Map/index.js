import React, { Component, Fragment } from 'react'
import { View, Image, Text, TouchableOpacity, Platform } from 'react-native'
import MapView, { Marker } from 'react-native-maps'
import Geocoder from 'react-native-geocoding'
import Reactotron from 'reactotron-react-native'

import getPixelSize, { apikey } from '../../utils'

import Search from '../Search'
import Directions from '../Directions';
import Details from '../Details';

import { styles } from './styles';
import { assets } from '../../assets';

Geocoder.init(apikey)
class Map extends Component {

    state = {
        region: null,
        destination: null,
        duration: null,
        location: null,
    }

    async componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            async (ret) => {
                var latitude = ret.coords.latitude;
                var longitude = ret.coords.longitude;

                const response = await Geocoder.from({ latitude, longitude })
                const item = response.results[0];

                const address = item.formatted_address;
                const location = address.substring(0, address.indexOf(','))

                const latDelta = Number(item.geometry.viewport.northeast.lat) - Number(item.geometry.viewport.southwest.lat)
                const lngDelta = Number(item.geometry.viewport.northeast.lng) - Number(item.geometry.viewport.southwest.lng)

                this.setState({
                    location,
                    region: {
                        latitude,
                        longitude,
                        latitudeDelta: parseFloat(latDelta.toFixed(3)),
                        longitudeDelta: parseFloat(lngDelta.toFixed(3))
                    }
                })

                Reactotron.log({ 'ret': ret, 'state': this.state })
            },
            (error) => { Reactotron.warn({ 'error': error }) },
            {
                timeout: 30000,
                enableHighAccuracy: true
            }
        );

        navigator.geolocation.watchPosition((position) => {
            Reactotron.log({ 'position': position })
        });
    }

    handleLocationSelected = (data, { geometry }) => {
        const { location: { lat: latitude, lng: longitude } } = geometry

        this.setState({
            destination: {
                latitude,
                longitude,
                title: data.structured_formatting.main_text
            }
        })

        Reactotron.log({ 'data': data, 'geometry': geometry, 'state': this.state })
    }

    handleBack = () => {
        this.setState({ destination: null })
    }

    render() {
        const { region, destination, duration, location } = this.state

        Reactotron.log({ 'state': this.state })

        return (
            <View style={{ flex: 1 }}>
                <MapView
                    style={{ flex: 1 }}
                    region={region}
                    showsUserLocation
                    loadingEnabled
                    ref={el => (this.mapView = el)}
                >
                    {destination && (
                        <Fragment>
                            <Directions origin={region} destination={destination}
                                onReady={(result) => {
                                    Reactotron.log({ 'result': result })

                                    this.setState({ duration: Math.floor(result.duration) })

                                    this.mapView.fitToCoordinates(result.coordinates, {
                                        edgePadding: {
                                            right: getPixelSize(50),
                                            left: getPixelSize(50),
                                            top: getPixelSize(50),
                                            bottom: getPixelSize(350),
                                        }
                                    })
                                }}
                            />

                            <Marker coordinate={destination} anchor={{ x: 0, y: 0 }} image={assets.markerImage}>
                                <View style={styles.LocationBox}>
                                    <Text style={styles.LocationText}>{destination.title}</Text>
                                </View>
                            </Marker>

                            <Marker coordinate={region} anchor={{ x: 0, y: 0 }}>
                                <View style={styles.LocationBox}>
                                    <View style={styles.LocationTimeBox}>
                                        <Text style={styles.LocationTimeText}>{duration}</Text>
                                        <Text style={styles.LocationTimeTextSmall}>min</Text>
                                    </View>

                                    <Text style={styles.LocationText}>{location}</Text>
                                </View>
                            </Marker>

                        </Fragment>
                    )}
                </MapView>

                {destination ?
                    (
                        <Fragment>
                            <TouchableOpacity style={styles.Back} onPress={() => this.handleBack()}>
                                <Image source={assets.backImage} style={{ width: 40, height: 40 }} />
                            </TouchableOpacity>

                            <Details />
                        </Fragment>
                    ) : (
                        <Search onLocationSelected={this.handleLocationSelected} />
                    )
                }
            </View>
        )
    }
}

export default Map