import { StyleSheet, Platform } from 'react-native'

export const styles = StyleSheet.create({
    Container: {
        backgroundColor: '#FFF',
        height: 300,
        width: '100%',
        position: 'absolute',
        bottom: 0,
        shadowColor: '#000',
        shadowOffset: { x: 0, y: 0 },
        shadowOpacity: 0.2,
        shadowRadius: 10,
        elevation: 3,
        borderWidth: 1,
        borderColor: '#DDD',
        alignItems: 'center',
        padding: 20,
    },
    TypeTitle: {
        fontSize: 20,
        color: '#222',
    },
    TypeDescription: {
        fontSize: 14,
        color: '#666',
    },
    TypeImage: {
        height: 80,
        marginTop: 10,
        marginBottom: 10,
        marginRight: 0,
        marginLeft: 0,
    },
    RequestButton: {
        backgroundColor: '#222',
        justifyContent: 'center',
        alignItems: 'center',
        height: 44,
        alignSelf: 'stretch',
        marginTop: 10,
    },
    RequestButtonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 18,
    },
    Back: {
        position: 'absolute',
        left: 20,
        top: Platform.select({ ios: 60, android: 40 }),
    }
})