import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Text } from 'react-native'
import Reactotron from 'reactotron-react-native'

import { styles } from './styles';
import { assets } from '../../assets';

class Details extends Component {
    render() {
        return (
            <View style={styles.Container}>
                <Text style={styles.TypeTitle}>Popular</Text>
                <Text style={styles.TypeDescription}>Viagens baratas para o dia a dia</Text>

                <Image style={styles.TypeImage} source={assets.uberImage} style={{ width: 40, height: 40 }} />

                <Text style={styles.TypeTitle}>UberX</Text>
                <Text style={styles.TypeDescription}>R$ 6,00</Text>

                <TouchableOpacity style={styles.RequestButton} onPress={() => { Reactotron.warn('Solicitando...') }}>
                    <Text style={styles.RequestButtonText}>SOLICITAR UBERX</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Details
