import React, { Component } from 'react'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'

import { styles } from './styles';
import { apikey } from '../../utils';

class Search extends Component {

    state = {
        searchFocused: false
    }

    render() {

        const { searchFocused } = this.state
        const { onLocationSelected } = this.props

        return (
            <GooglePlacesAutocomplete
                placeholder="Para onde deseja ir?"
                placeholderTextColor="#333"
                onPress={onLocationSelected}
                query={{
                    key: apikey,
                    language: 'pt'
                }}
                textInputProps={{
                    autoCapitalize: 'none',
                    autoCorrect: false,
                    onFocus: () => { this.setState({ searchFocused: true }) },
                    onBlur: () => { this.setState({ searchFocused: false }) }
                }}
                listViewDisplayed={searchFocused}
                fetchDetails
                enablePoweredByContainer={false}
                styles={styles}
            />
        )
    }
}

export default Search